const {request, response} = require("express");
const express = require("express");

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// 1-2
app.get("/home", (request, response) => {
    response.send("Welcome to home page!");
});



// 3-4
let users = [{
    username: "johndoe",
    password: "johndoe1234",
},
{
    username: "janesmith",
    password: "johndoe1234",
}];

app.get("/users", (request, response) => {
    response.send(users);
});



// 
app.delete("/delete-user", (request, response) => {
    let message = "";

    for(let i = 0; i < users.length; i++) {
        if(request.body.username == users[i].username){
            users[i].username = request.body.username;

            message = `User ${request.body.username} has been deleted.`;
            break
        } else {
            message = "No user found"
        }
    }

    response.send(message);
})





app.listen(port, () => console.log(`Server is running at port ${port}!`))